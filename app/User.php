<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'surnames','role', 'email', 'country_id','document_id', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    // Relationships
    public function votes(){
      return $this->hasOne('App\Vote');
    }

    public function country(){
      return $this->belongsTo('App\Country');
    }


    protected $hidden = ['password', 'remember_token'];

    // set and encryp password
    public function setPasswordAttribute($value){
      if (!empty($value)) {
        $this->attributes['password'] = \Hash::make($value);
      }
    }
}
