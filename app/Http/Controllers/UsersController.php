<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\User;
use App\Country;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{   
	public function __construct(){
	  $this->beforeFilter('@find',['only' => ['edit', 'update', 'destroy']]);
	  $this->middleware('auth', ['only' => ['edit', 'update', 'destroy']]);
	  $this->middleware('admin', ['only' => ['index','edit', 'update', 'destroy']]);
	}

	public function find(Route $route)
	{
	  $this->user = User::find($route->getParameter('users'));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	  $users = User::All();
	  return view('users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$country = Country::All()->lists('name','id');
		return view('users.create', compact('country'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(UserCreateRequest $request)
	{
		// return $request->all();
		User::create($request->all());
		if($request['role'] == 2){
			return redirect('/users')->with('message', 'User create');
		}else{
			return redirect('/admin')->with('message', 'User create');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$country = Country::All()->lists('name','id');
	  return view('users.edit', ['user' => $this->user,'country'=> $country]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UserUpdateRequest $request, $id)
	{
	  $this->user->fill($request->all());
	  $this->user->save();

	  Session::flash('message', 'User edit success');
	  return Redirect::to('/users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	  User::destroy($id);
	  Session::flash('message', 'User destroy success');
	  return Redirect::to('/users');
	}
}
