<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Committee;
use App\Country;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Requests\CommitteeRequest;
use App\Http\Controllers\Controller;

class CommitteesController extends Controller
{   
  public function __construct(){
    $this->beforeFilter('@find',['only' => ['edit', 'update', 'destroy']]);
    $this->middleware('auth');
    $this->middleware('admin');
  }

  public function find(Route $route)
  {
    $this->committee = Committee::find($route->getParameter('committees'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $committees = Committee::All();
    return view('committees.index', compact('committees'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $country = Country::All()->lists('name','id');
    return view('committees.create', compact('country'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CommitteeRequest $request)
  {
    Committee::create($request->all());
    return redirect('/committees')->with('message', 'Committee create');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $country = Country::All()->lists('name','id');
    return view('committees.edit', ['committee' => $this->committee,'country'=> $country]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CommitteeRequest $request, $id)
  {
    $this->committee->fill($request->all());
    $this->committee->save();

    Session::flash('message', 'User edit success');
    return Redirect::to('/committees');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Committee::destroy($id);
    Session::flash('message', 'User destroy success');
    return Redirect::to('/committees');
  }
}
