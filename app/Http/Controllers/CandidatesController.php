<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Candidate;
use App\Country;
use App\Committee;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Requests\CandidateRequest;
use App\Http\Controllers\Controller;

class CandidatesController extends Controller
{
  public function __construct(){
    $this->beforeFilter('@find',['only' => ['edit', 'update', 'destroy']]);
    $this->middleware('auth');
    $this->middleware('admin');
  }

  public function find(Route $route)
  {
    $this->candidate = Candidate::find($route->getParameter('candidates'));
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $candidates = Candidate::All();
    return view('candidates.index', compact('candidates'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $country = Country::All()->lists('name','id');
    $committee = Committee::All()->lists('name','id');
    return view('candidates.create',['committee' => $committee,'country'=> $country]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CandidateRequest $request)
  {
    Candidate::create($request->all());

    return redirect('/candidates')->with('message', 'Candidate create');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $country = Country::All()->lists('name','id');
    $committee = Committee::All()->lists('name','id');
    return view('candidates.edit', ['candidate' => $this->candidate,'committee' => $committee,'country'=> $country]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(CandidateRequest $request, $id)
  {
    $this->candidate->fill($request->all());
    $this->candidate->save();

    Session::flash('message', 'candidate edit success');
    return Redirect::to('/candidates');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Candidate::destroy($id);
    Session::flash('message', 'candidate destroy success');
    return Redirect::to('/candidates');
  }
}
