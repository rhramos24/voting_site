<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FrontController@index');
Route::get('admin','FrontController@admin');
Route::get('user','FrontController@user');

Route::resource('countries', 'CountriesController');
Route::resource('committees', 'CommitteesController');
Route::resource('candidates', 'CandidatesController');
Route::resource('votes', 'VotesController');
Route::resource('users', 'UsersController');

Route::resource('log', 'LogController');
Route::get('logout','LogController@logout');

