<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class Committee extends Model {
  
  protected $table = 'committees';

  protected $fillable = ['name', 'country_id'];

  // Relationships
  public function country(){
    return $this->belongsTo('App\Country');
  }

  public function candidates(){
    return $this->hasMany('App\Candidate');
  }


}