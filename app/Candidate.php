<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model{

  protected $table = 'candidates';

  protected $fillable = ['name', 'surnames', 'email', 'document_id', 'country_id', 'committee_id', 'department'];

  // Relationships
  public function votes(){
    return $this->hasMany('App\Vote');
  }

  public function committee(){
    return $this->belongsTo('App\Committee');
  }

  public function country(){
    return $this->belongsTo('App\Country');
  }
  
}
