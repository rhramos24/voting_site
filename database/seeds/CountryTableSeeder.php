<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
      DB::table('countries')->delete();
      Country::create(array('name' => 'El Salvador'));
    }
}
